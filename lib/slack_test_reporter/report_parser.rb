# frozen_string_literal: true

module SlackTestReporter
  # Class to Unmarshall a dataset into
  class ReportParser
    def self.parse(file)
      "parsing: #{file}"
    end
  end
end
