# frozen_string_literal: true

require 'optparse'
require 'ostruct'
require 'active_support/inflector'

require 'slack_test_reporter'

module SlackTestReporter
  class Runner
    def self.run(args)
      # Usage: ./slack-test-reporter --format junit rspec-junit-results.xml (stdout: Slack JSON)
      options = OpenStruct.new(format: :junit, file: nil, output: $stdout, verbose: false)

      OptionParser.new do |opts|
        opts.banner = "Usage: #{$PROGRAM_NAME} [--format FORMAT] input_file"

        opts.on('-f', '--format FORMAT', 'The format to parse (default: junit)') do |format|
          $stdout.puts "Format: #{format}" if options[:verbose]

          options[:format] = format
        end

        opts.on('-o', '--out FILE', 'Output result of Slack JSON to a file') do |out|
          begin
            $stdout.puts "Outputting to file: #{out}" if options[:verbose]

            options[:output] = File.open(out, 'w')
          rescue
            $stderr.puts "Could not open file #{out} for writing!"
            exit 3
          end
        end

        opts.on_tail('--version', 'Show the version') do
          $stdout.puts "#{$PROGRAM_NAME} : #{SlackTestReporter::VERSION}"
          exit
        end

        opts.on_tail('-h', '--help', 'Show usage') do
          $stdout.puts opts
          exit
        end

        opts.on_tail('-v', '--verbose', 'Toggle output verbosity') { |v| options[:verbose] = !!v }

        opts.parse(args)

        file_name = args.pop

        $stdout.puts "Input file: #{file_name}" if options[:verbose]

        begin
          raise 'Missing argument for input file!' unless file_name

          $stdout.puts "Opening #{file_name}..." if options[:verbose]
          options[:file] = File.open(file_name, 'r')
        rescue Errno::ENOENT => e
          $stderr.puts "Couldn't open file #{file_name} for reading! #{e.message}"
          exit 2
        ensure
          options[:file]&.close
        end
      end


      parser = SlackTestReporter::Parser.const_get("#{options[:format]}".classify)
      $stdout.puts <<~OUT if options[:verbose]
        Running Parser #{parser} with options:
          #{options.marshal_dump}
      OUT

      options[:output].puts parser.parse(options[:file])

      $stdout.puts "...Ended run" if options[:verbose]
    ensure
      options[:file]&.close
      options[:output]&.close
    end
  end
end
