# frozen_string_literal: true

require 'slack_test_reporter'
require 'factory_bot'
require 'rspec-parameterized'

RSpec.configure do |rspec|
  rspec.order = :random
  rspec.disable_monkey_patching!

  rspec.include FactoryBot::Syntax::Methods

  rspec.before(:suite) do
    FactoryBot.find_definitions
  end
end
