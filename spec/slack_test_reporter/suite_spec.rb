# frozen_string_literal: true

module SlackTestReporter
  RSpec.describe Suite do
    describe 'attributes' do
      it { should respond_to :name, :time, :total_count, :total_successes, :total_failures, :total_skipped, :total_errors, :error_message }
      it { should respond_to :name=, :time=, :total_count=, :total_successes=, :total_failures=, :total_skipped=, :total_errors=, :error_message= }
      it { should respond_to :results, :results= }
    end
  end
end
